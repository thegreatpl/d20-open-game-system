﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20OpenGameSystem.Entities
{
    public class BaseEntity
    {
        //Attributes. 

        public int Strength { get; protected set; }

        public int Dexterity { get; protected set; }

        public int Constitution { get; protected set; }

        public int Intelligence { get; protected set; }

        public int Charisma { get; protected set; }

        public int Wisdon { get; protected set; }

        //Health. 

        public int MaxHealth { get; protected set; }

        public int CurrentHealth { get; protected set; }

        public bool Stabalized { get; protected set; }

        //positions

        public int Xpos { get; protected set; }

        public int Ypos { get; protected set; }

        public DirectionEnums Facing { get; protected set; }

        protected D20OpenGameSystem.Map.BaseMap CurrentMap { get; private set; }

        public BaseEntity()
        {
            Xpos = 10;
            Ypos = 10;
            Facing = DirectionEnums.forwards; 
        }

        /// <summary>
        /// Sets the map the entity is currently on. The entity will call against this. 
        /// </summary>
        /// <param name="NewCurrentMap"></param>
        public void SetMap (D20OpenGameSystem.Map.BaseMap NewCurrentMap)
        {
            CurrentMap = NewCurrentMap; 
        }

        /// <summary>
        /// returns the bonus from attribute. 
        /// </summary>
        /// <param name="AttributeValue">The attribute to recieve a bonus from</param>
        /// <returns></returns>
        public int GetAttributeBonus (int AttributeValue)
        {
            return (AttributeValue - 10) / 2; 
        }

        /// <summary>
        /// General update function for an Entity. 
        /// </summary>
        public void Update()
        {

        }

        /// <summary>
        /// Moves the entity in the direction told. 
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        public void Move (DirectionEnums direction)
        {
            Facing = direction; 
            switch (direction)
            {
                case DirectionEnums.backwards:
                    if (CurrentMap.TilePassable(Xpos, Ypos - 1))
                    {
                        Ypos--;
                    }
                    break;
                case DirectionEnums.forwards:
                    if (CurrentMap.TilePassable(Xpos, Ypos + 1))
                    {
                        Ypos++;
                    }
                    break;

                case DirectionEnums.left:
                    if (CurrentMap.TilePassable(Xpos - 1, Ypos))
                    {
                        Xpos--;
                    }
                    break;

                case DirectionEnums.right:
                    if (CurrentMap.TilePassable(Xpos + 1, Ypos))
                    {
                        Xpos++;
                    }
                    break;
            }
        }
    }
}
