﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20OpenGameSystem.Entities
{
    public enum DirectionEnums
    {
        forwards,
        backwards,
        left, 
        right
    }
}
