﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20OpenGameSystem.Map
{
    public class BaseMap
    {
        public Tile[,] Map { get; protected set; }
        public int XSize { get; protected set; }
        public int YSize { get; protected set; }

        public BaseMap()
        {
            XSize = 100;
            YSize = 100;
            Map = new Tile[XSize, YSize];
            for (int xdx = 0; xdx < XSize; xdx++)
            {
                for (int ydx = 0; ydx < YSize; ydx++)
                {
                    Map[xdx, ydx] = new Tile(); 
                }
            }

            Map[50, 50].SetPassability(false);
        }

        /// <summary>
        /// Main update function of the map. 
        /// </summary>
        public void Update()
        {

        }

        /// <summary>
        /// Checks whether or not the position is passable or not. 
        /// Will return false if the values given are outside of the Map. 
        /// </summary>
        /// <param name="Xpos">The X coordinate</param>
        /// <param name="Ypos">The Y Coordinate</param>
        /// <returns></returns>
        public bool TilePassable (int Xpos, int Ypos)
        {
            if (!(Xpos >= 0 && Xpos < XSize ))
            {
                return false; 
            }
            if (!(Ypos >= 0 && Ypos < YSize))
            {
                return false; 
            }

            return Map[Xpos, Ypos].Passable; 
        }
    }
}
