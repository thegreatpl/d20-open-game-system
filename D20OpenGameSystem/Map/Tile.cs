﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D20OpenGameSystem.Map
{
    public class Tile
    {
        public bool Passable { get; protected set; }

        public Tile()
        {
            Passable = true; 
        }

        public void SetPassability(bool newPassable)
        {
            Passable = newPassable; 
        }
    }
}
