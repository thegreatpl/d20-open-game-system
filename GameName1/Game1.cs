﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

using D20OpenGameSystem;
using D20OpenGameSystem.Entities;
using D20OpenGameSystem.Map; 
#endregion

namespace GameName1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        BaseEntity player;
        BaseMap Map;

        Texture2D playerText; 

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            player = new BaseEntity();
            Map = new BaseMap();
            player.SetMap(Map); 
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            playerText = Content.Load<Texture2D>("pixel");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                player.Move(DirectionEnums.backwards);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                player.Move(DirectionEnums.forwards);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                player.Move(DirectionEnums.left);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                player.Move(DirectionEnums.right);
            }

            player.Update(); 
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int tileWidth = 5; 
            spriteBatch.Begin();
            for (int xdx = 0; xdx < Map.XSize; xdx++)
            {
                for (int ydx = 0; ydx < Map.YSize; ydx++)
                {
                    if (Map.Map[xdx, ydx].Passable)
                    {
                        spriteBatch.Draw(playerText, new Rectangle(xdx * tileWidth, ydx * tileWidth, tileWidth, tileWidth), Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(playerText, new Rectangle(xdx * tileWidth, ydx * tileWidth, tileWidth, tileWidth), Color.Black);
                    }
                }
            }

                
            spriteBatch.Draw(playerText, new Rectangle(player.Xpos * tileWidth, player.Ypos * tileWidth, tileWidth, tileWidth), Color.Red);
            spriteBatch.End(); 
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
